/**
 * 
 */
function ClickStreamVars() {
	var oyp = this;
	var oyp_mapToProducts = [ "cardservicesproduct", "item", "mraddtowishlist",
			"mrcarttowishlist", "mrcheckout", "mrorder", "mrpointspurchased",
			"mrprodremove", "mrprodselect", "mrprodview", "mrrelatedproduct",
			"mrwishlisttocart", "products", "regcomplete", "regstart",
			"selfservice", "selfservicecomplete", "selfservicestart",
			"selfservicetype", "psprod" ];

	oyp.get_cookie = function(c_name) {

		var a = document.cookie.split(';');

		for (var i = 0; i < a.length; i++) {
			var c = a[i];
			while (c.charAt(0) == ' ')
				c = c.substring(1, c.length);
			if (c.indexOf(c_name) >= 0)
				return c;
		}
		return null;
	};

	oyp.getQueryParameterByName = function(para_name) {
		para_name = para_name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + para_name + "(=([^&#]*)|&|#|$)"), results = regex
				.exec(window.location.href);
		if (!results)
			return null;
		if (!results[2])
			return '';
		
		if(results.indexOf("<") > -1 || results.indexOf(">") > -1){
			return "malicious content";
		}
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	};
    oyp.get_prMdAvl = function() {
        try {
            return (typeof window.userInfo === "object");
        } catch (err) {
            //console.log("no Visitor on page to capture mmpId");
        }
        return false;
    };
    oyp.get_prMdInd = function() {
    	var retVar = undefined;
        try {
            if (typeof window.userInfo === "object") {
            	return window.userInfo.isPrivate;
            }
        } catch (err) {
            retVar = undefined;
            //console.log("no window.userInfo.isPrivate on page to capture");
        }
        return retVar;
    };
    oyp.get_reqId = function() {
        var retVar = undefined;
        try {
            if (typeof window.digitalData === "object") {
                if (typeof window.digitalData.page === "object") {
                    if (typeof window.digitalData.page.attributes === "object") {
                        retVar =  window.digitalData.page.attributes.requestId;
                    }
                }
			}
        } catch (err) {
            retVar = undefined;
            //console.log("no window.digitalData.page.attributes.requestId on page to capture");
        }
        return retVar;
    };
	oyp.get_mmpid = function() {
		var oyp_mmpId = "";
		var vis = null;
		try {
			if (typeof window._satellite === "object") {
				vis =  _satellite.getVisitorId();
			} else if (typeof window.Visitor === "function" && typeof window.visitor === "object") {
				vis = window.visitor;
			}
			
			if (vis != null && typeof vis.getMarketingCloudVisitorID() != "undefined") {
				oyp_mmpId = vis.getMarketingCloudVisitorID();
			}			
			
			return oyp_mmpId;
		} catch (err) {
			//console.log("no Visitor on page to capture mmpId");
		}
	};
	
	oyp.get_public_Guid = function() {
		var oyp_publicGuid = null;
		var oyp_blueboxpublic_ck = this.get_cookie("blueboxpublic");

		if (oyp_blueboxpublic_ck) {
			starting_index = oyp_blueboxpublic_ck.indexOf("blueboxpublic=")
					+ "blueboxpublic=".length;
			oyp_publicGuid = oyp_blueboxpublic_ck.substring(starting_index,
					oyp_blueboxpublic_ck.length);
		}

		return oyp_publicGuid;
	};
	
	oyp.getOnlypagename = function() {
	    var oyp_pgname = null;
	    if (typeof (omn) == "undefined") {
	        oyp_pgname = window.omn_pagename;
	    } else {
	    	if(omn.hierarchy != null && omn.hierarchy != "undefined"){
	    		oyp_pgname = omn.hierarchy + "|" + omn.pagename;
	    	}else{
	    		oyp_pgname = omn.pagename;
	    	}
	    }
	    return oyp_pgname;
	};

	oyp.getproducts = function() {
		var oyp_products = "";
		
		if(typeof (omn) != "undefined"){
			for ( var m in oyp_mapToProducts) {
				pv = oyp_mapToProducts[m];
				try{
					var aNames = Object.getOwnPropertyNames(omn);
					if (aName && aName.indexOf(pv) > -1 ) {
						prodv = omn[pv];
						if (prodv && prodv.charAt(0) !== ";") {
							prodv = ";" + prodv;
						}
						oyp_products = oyp_products + prodv;
					}
				}catch(ex){}
			}
			if (oyp_products.charAt(0) == ";") {
				oyp_products = oyp_products.substring(1, oyp_products.length);
			}
		}else {
			oyp_products = window.omn_products == undefined?null:window.omn_products;
		}
		return oyp_products;
	};

	oyp.get_gctid = function() {
		var oyp_gctrac_ck = this.get_cookie("gctracus=");
		var oyp_gctId = null;
		if (oyp_gctrac_ck && oyp_gctrac_ck.indexOf("gctvid=") >= 0) {
			var var_len = 60;
			initial_index = oyp_gctrac_ck.indexOf("gctvid=") + "gctvid=".length;
			oyp_gctId = oyp_gctrac_ck.substring(initial_index, initial_index + var_len);
			var ei = oyp_gctId.indexOf("&");
			if(ei > 0) {
				oyp_gctId = oyp_gctId.substring(0, ei);
			}
		}
		return oyp_gctId;
	};

	oyp.get_ppvs = function() {

		var oyp_ppvs = [];
		var oyp_session_cookie = this.get_cookie("s_sess=");

		if (oyp_session_cookie) {
			var a = decodeURIComponent(oyp_session_cookie);
			var b = decodeURIComponent(a);
			var c = decodeURIComponent(b);
			var session_vars = c.split(";");

			for (var i = 0; i < session_vars.length; i++) {
				var v = session_vars[i];
				while (v.charAt(0) == ' ')
					v = v.substring(1, v.length);

				if (v.indexOf("s_ppv=") >= 0) {
					initial_index = v.indexOf("s_ppv=") + "s_ppv=".length;
					v = v.substring(initial_index, v.length);
					oyp_ppvs = v.split(",");
					// console.log(ppvs);
					break;
				}
			}
		}
		return oyp_ppvs;
	};

	oyp.get_in_vars = function() {
		var oyp_inav = null;
		var oyp_intlink = null;

		var oyp_session_cookie = this.get_cookie("s_sess=");

		if (oyp_session_cookie) {
			var a = decodeURIComponent(oyp_session_cookie);
			var b = decodeURIComponent(a);
			var c = decodeURIComponent(b);
			var session_vars = c.split(";");

			for (var i = 0; i < session_vars.length; i++) {
				var v = session_vars[i];
				while (v.charAt(0) == ' ')
					v = v.substring(1, v.length);

				if (v.indexOf("omn_inav=") >= 0) {
					var starting_point = v.indexOf("omn_inav=")
							+ "omn_inav=".length;
					oyp_inav = v.substring(starting_point, v.length);
				}

				if (v.indexOf("omn_intlink=") >= 0) {
					starting_point = v.indexOf("omn_intlink=")
							+ "omn_intlink=".length;
					oyp_intlink = v.substring(starting_point, v.length);
				}
			}

			if (!oyp_inav) {
				// not defined in omn or window.omn_inav
				inav_in_url = this.getQueryParameterByName("inav");
				if (inav_in_url != null) {
					oyp_inav = inav_in_url;
				}
			}

			if (!oyp_intlink) {

				if (typeof (omn) == "undefined") {
					oyp_intlink = window.omn_intlink;
				} else {
					oyp_intlink = omn.intlink;
				}

				if (!oyp_intlink) { // undefined in omn variables

					intlink_in_url = this.getQueryParameterByName("intlink");

					if (intlink_in_url != null) {
						oyp_intlink = intlink_in_url;
					}
				}

			}
		}
		var oyp_invars = [ oyp_inav, oyp_intlink ];
		return oyp_invars;
	};

	oyp.getplugins = function() {
		var p = "";
		var oyp_navplugins = navigator.plugins;
		if (oyp_navplugins) {
			for (var i = 0; i < oyp_navplugins.length && i < 30; i++) {
				if (plugin_name = oyp_navplugins[i].name) {
					plugin_name = plugin_name.substring(0, 100) + ";", p
							.indexOf(plugin_name) < 0
							&& (p += plugin_name);
				}

			}
		}

		return p;
	};

	oyp.get_attribute_array = function() {

		var oyp_attribute_array = [];

		var oyp_pagename = this.getOnlypagename();
		var oyp_page_name = {
			"key" : "d_pageName",
			"value" : oyp_pagename
		};
		oyp_attribute_array.push(oyp_page_name);

		var oyp_products = this.getproducts();
		oyp_products = oyp_products == "" ? null : oyp_products;

		var product_list = {
			"key" : "d_products",
			"value" : oyp_products
		};
		oyp_attribute_array.push(product_list);

		if (typeof (omn) == "undefined") { // use legacy way

			var oyp_pageId = window.omn_PageId == undefined ? null
					: window.omn_PageId;

			if (oyp_pageId == null) {
				if(typeof (window.$itag) != "undefined") {
					oyp_pageId = window.$itag.PageId == undefined ? null
						: window.$itag.PageId;
				}
			}

			var oyp_page_id_obj = {
				"key" : "d_pageId",
				"value" : oyp_pageId
			};
			oyp_attribute_array.push(oyp_page_id_obj);

			var oyp_language = window.omn_language == undefined ? null
					: window.omn_language;
			var lang = {
				"key" : "d_language",
				"value" : oyp_language
			};
			oyp_attribute_array.push(lang);

			var oyp_abtest = window.omn_abtest == undefined ? null
					: window.omn_abtest;
			var ab_test = {
				"key" : "d_abTest",
				"value" : oyp_abtest
			};
			oyp_attribute_array.push(ab_test);

			var oyp_events = window.omn_events == undefined ? null
					: window.omn_events;
			var event_list = {
				"key" : "d_events",
				"value" : oyp_events
			};
			oyp_attribute_array.push(event_list);

			// intlinkimp not legacy parameter (omn_intlinkimp)
			var oyp_intlinkimp = this.getQueryParameterByName("intlinkimp");

			if (oyp_intlinkimp == null || oyp_intlinkimp == "") {
				if (typeof (itag_intlinkimp) != "undefined") {
					oyp_intlinkimp = itag_intlinkimp;
				}
			}

			var oyp_intlink_imp = {
				"key" : "d_intLinkImp",
				"value" : oyp_intlinkimp
			};
			oyp_attribute_array.push(oyp_intlink_imp);

			// linknav not legacy parameter (omn_linknav)

			var oyp_Linknav = this.getQueryParameterByName("linknav");
			var link_nav = {
				"key" : "d_linkNav",
				"value" : oyp_Linknav
			};
			oyp_attribute_array.push(link_nav);

			var oyp_pcnnumber = window.omn_pcnnumber == undefined ? null
					: window.omn_pcnnumber;
			var pcn = {
				"key" : "d_pcnNumber",
				"value" : oyp_pcnnumber
			};
			oyp_attribute_array.push(pcn);

		} else { // use new way

			var oyp_pageId = omn.PageId == undefined ? null : omn.PageId;

			if (oyp_pageId == null) {
				if(typeof (window.$itag) != "undefined") {
					oyp_pageId = window.$itag.PageId == undefined ? null
						: window.$itag.PageId;
				}
			}

			var page_id = {
				"key" : "d_pageId",
				"value" : oyp_pageId
			};
			oyp_attribute_array.push(page_id);

			var oyp_language = omn.language == undefined ? null : omn.language;
			var lang = {
				"key" : "d_language",
				"value" : oyp_language
			};
			oyp_attribute_array.push(lang);

			var oyp_abtest = omn.abtest == undefined ? null : omn.abtest;
			var ab_test = {
				"key" : "d_abTest",
				"value" : oyp_abtest
			};
			oyp_attribute_array.push(ab_test);

			var oyp_events = omn.events == undefined ? null : omn.events;
			var event_list = {
				"key" : "d_events",
				"value" : oyp_events
			};
			oyp_attribute_array.push(event_list);

			var oyp_intlinkimp = omn.intlinkimp;
			if (oyp_intlinkimp == undefined) {
				oyp_intlinkimp = this.getQueryParameterByName("intlinkimp");
			}

			if (oyp_intlinkimp == null || oyp_intlinkimp == "") {
				if (typeof (itag_intlinkimp) != "undefined") {
					oyp_intlinkimp = itag_intlinkimp;
				}
			}

			var intlink_imp = {
				"key" : "d_intLinkImp",
				"value" : oyp_intlinkimp
			};
			oyp_attribute_array.push(intlink_imp);

			var oyp_Linknav = omn.linknav;
			if (oyp_Linknav == undefined) {
				oyp_Linknav = this.getQueryParameterByName("linknav");
			}
			var link_nav = {
				"key" : "d_linkNav",
				"value" : oyp_Linknav
			};
			oyp_attribute_array.push(link_nav);

			var oyp_pcnnumber = omn.pcnnumber == undefined ? null
					: omn.pcnnumber;
			var pcn = {
				"key" : "d_pcnNumber",
				"value" : oyp_pcnnumber
			};
			oyp_attribute_array.push(pcn);
			
		}
		

		//var oyp_market = this.getMarket();
		var oyp_market = "US";
		var mkt = {
			"key" : "d_market",
			"value" : oyp_market
		};
		oyp_attribute_array.push(mkt);

		var oyp_ppvs = this.get_ppvs();

		var oyp_ppvpage = null;
		var oyp_ppvtotal = null;
		var oyp_ppvinitial = null;

		if (oyp_ppvs.length > 3) {
			oyp_ppvpage = oyp_ppvs[0];
			oyp_ppvtotal = oyp_ppvs[1];
			oyp_ppvinitial = oyp_ppvs[2];
		}

		var oyp_ppv_page = {
			"key" : "d_ppvPage",
			"value" : oyp_ppvpage
		};
		var oyp_ppv_total = {
			"key" : "d_ppvTotal",
			"value" : oyp_ppvtotal
		};
		var oyp_ppv_initial = {
			"key" : "d_ppvInitial",
			"value" : oyp_ppvinitial
		};
		oyp_attribute_array.push(oyp_ppv_page);
		oyp_attribute_array.push(oyp_ppv_total);
		oyp_attribute_array.push(oyp_ppv_initial);
		var oyp_invars = this.get_in_vars();
		var oyp_inav = oyp_invars[0];
		var oyp_intlink = oyp_invars[1];

		var oyp_i_nav = {
			"key" : "d_iNav",
			"value" : oyp_inav
		};
		var oyp_int_link = {
			"key" : "d_intLink",
			"value" : oyp_intlink
		};
		oyp_attribute_array.push(oyp_i_nav);
		oyp_attribute_array.push(oyp_int_link);

		var oyp_referrer = document.referrer == undefined ? null
				: document.referrer;
		
		if(oyp_referrer){
			
			if(oyp_referrer.indexOf("<") > -1 || oyp_referrer.indexOf(">") > -1){
				oyp_referrer = "malicious referrer url";
			} else if (oyp_referrer) {
				oyp_referrer = oyp_referrer.split("?")[0];
			} else {
				oyp_referrer = null;
			}
		}
		
		var oyp_ref = {
			"key" : "d_referrer",
			"value" : oyp_referrer
		};
		oyp_attribute_array.push(oyp_ref);

		var oyp_url = window.location.href == undefined ? null
				: window.location.href;
		
		if(oyp_url){
			if(oyp_url.indexOf("<") > -1 || oyp_url.indexOf(">") > -1){
				oyp_url = "malicious_url";
			} else {
				oyp_url = oyp_url.split("?")[0];
			}
		}
		
		var oyp_url_obj = {
			"key" : "d_url",
			"value" : oyp_url
		};
		oyp_attribute_array.push(oyp_url_obj);
		var oyp_dt = new Date();
		var oyp_timestamp = Math.floor(oyp_dt.getTime() / 1000);
		var t = {
			"key" : "d_ts",
			"value" : oyp_timestamp
		};
		oyp_attribute_array.push(t);
		var t1 = {
			"key" : "d_tzo",
			"value" : oyp_dt.getTimezoneOffset()
		};
		oyp_attribute_array.push(t1);
		var oyp_plugins = this.getplugins();
		oyp_plugins = oyp_plugins ? oyp_plugins : null;
		var p = {
			"key" : "d_plugins",
			"value" : oyp_plugins
		};
		oyp_attribute_array.push(p);

		var oyp_publicGuid = this.get_public_Guid();
		var public_guid = {
			"key" : "d_publicGuId",
			"value" : oyp_publicGuid
		};
		oyp_attribute_array.push(public_guid);
		
		var mmp_id = this.get_mmpid();
		var oyp_mmpid = {
				"key" : "d_mid",
				"value" : mmp_id
		};
		oyp_attribute_array.push(oyp_mmpid);

        var prMdAvl = this.get_prMdAvl();
        var oyp_prMdAvl = {
            "key" : "d_prMdAvl",
            "value" : prMdAvl
        };
        oyp_attribute_array.push(oyp_prMdAvl);

        var prMdInd = this.get_prMdInd();
        var oyp_prMdInd = {
            "key" : "d_prMdInd",
            "value" : prMdInd
        };
        oyp_attribute_array.push(oyp_prMdInd);

        var reqId = this.get_reqId();
        var oyp_reqId = {
            "key" : "d_reqId",
            "value" : reqId
        };
        oyp_attribute_array.push(oyp_reqId);

		var oyp_bw = {
				"key" : "d_bw",
				"value" : window.innerWidth
		};
		oyp_attribute_array.push(oyp_bw);
		
		var oyp_bh = {
				"key" : "d_bh",
				"value" : window.innerHeight
		};
		
		oyp_attribute_array.push(oyp_bh);
		
		var oyp_sr = {
				"key" : "d_sr",
				"value" : screen.width +"x" +screen.height
		};
		oyp_attribute_array.push(oyp_sr);
		
		
		if (typeof(digitalData) != "undefined") {
			
			if(typeof(digitalData.user) != "undefined"){
				
				if(oyp_attribute_array[8].key == "d_pcnNumber" && (oyp_attribute_array[8].value == null || oyp_attribute_array[8].value == "undefined")){
					oyp_attribute_array[8].value = digitalData.user.pcnNumber;
				}
			}	
				
			if(typeof(digitalData.event) != "undefined" && digitalData.event.length > 0 ){
				
				if(oyp_attribute_array[5].key == "d_events" && (oyp_attribute_array[5].value == null || oyp_attribute_array[5].value == "undefined") 
						&& Object.keys(digitalData.event[0]).indexOf("eventInfo") > -1){
					
					var eventDt = "";
					Object.getOwnPropertyNames(digitalData.event[0].eventInfo).forEach(function(val, idx, array) {
						eventDt = eventDt + digitalData.event[0].eventInfo[val]+"|";
					});
					if(eventDt && eventDt.length > 0){
						eventDt = eventDt.substring(0,eventDt.length-1)
					}
					oyp_attribute_array[5].value = eventDt;
				}
				try {
					if(oyp_attribute_array[1].key == "d_products" && (oyp_attribute_array[1].value == null || oyp_attribute_array[1].value == "undefined")){
						if(Object.getOwnPropertyNames(digitalData.event[0]).indexOf("productInfo") > -1 && digitalData.event[0].productInfo.length > 0 
								&& Object.getOwnPropertyNames(digitalData.event[0].productInfo[0]).indexOf("productName") > -1) {
							oyp_attribute_array[1].value = digitalData.event[0].productInfo[0].productName;
						}
					}
				}catch(ex){
					//console.log("couldn't find product.");
				}
			}
			
			if(typeof(digitalData.page) != "undefined"){
				
				if(typeof(digitalData.page.pageInfo)!= "undefined") {
					
					if(oyp_attribute_array[0].key == "d_pageName" && (oyp_attribute_array[0].value == null || oyp_attribute_array[0].value == "undefined") 
							&& typeof(digitalData.page.category) != "undefined") {
						
						var prefix = digitalData.page.pageInfo.country + "|";
						Object.getOwnPropertyNames(digitalData.page.category).forEach(function(val, idx, array) {
							prefix = prefix + digitalData.page.category[val]+"|";
						});
						if(digitalData.page.pageInfo.pageName && digitalData.page.pageInfo.pageName.length > 0){
							prefix = prefix + digitalData.page.pageInfo.pageName;
						}else{
							if(prefix && prefix.length > 0){
								prefix = prefix.substring(0,prefix.length-1)
							}
						}
						oyp_attribute_array[0].value = prefix;
					}
					
					if(oyp_attribute_array[2].key == "d_pageId" && (oyp_attribute_array[2].value == null || oyp_attribute_array[2].value == "undefined")){
						oyp_attribute_array[2].value = digitalData.page.pageInfo.pageID;
					}
					
					if(oyp_attribute_array[3].key == "d_language" && (oyp_attribute_array[3].value == null || oyp_attribute_array[3].value == "undefined")){
						oyp_attribute_array[3].value = digitalData.page.pageInfo.language;
					}
				}
				
				if(typeof(digitalData.page.attributes) != "undefined"){
					
					if(oyp_attribute_array[4].key == "d_abTest" && (oyp_attribute_array[4].value == null || oyp_attribute_array[4].value == "undefined")){
						oyp_attribute_array[4].value = digitalData.page.attributes.abTest;
					}
					
					if(oyp_attribute_array[6].key == "d_intLinkImp" && digitalData.page.attributes.hasOwnProperty("intlinkimp") && (oyp_attribute_array[6].value == null || oyp_attribute_array[6].value == "undefined")){
						oyp_attribute_array[6].value = digitalData.page.attributes["intlinkimp"];
					}
					
					if(oyp_attribute_array[7].key == "d_linkNav" && (oyp_attribute_array[7].value == null || oyp_attribute_array[7].value == "undefined")){
						oyp_attribute_array[7].value = digitalData.page.attributes.linknav;
					}
				}
			}
		}

		return oyp_attribute_array;
	};


	/**
	 * Not breaking json format of data, instead would read it and will create
	 * query parameters.
	 * 
	 * @returns
	 */
	oyp.getClickStreamData = function() {
		var oyp_query_params = [];
		var oyp_gctId = this.get_gctid();

		oyp_query_params.push("d_gctId=" + encodeURIComponent(oyp_gctId));
		
		var oyp_attribute_array = this.get_attribute_array();

		len = oyp_attribute_array.length;
		for (var i = 0; i < len; i++) {
			var oyp_attribute = oyp_attribute_array[i];
			if(oyp_attribute.key == "d_mid" && oyp_attribute.value == null){
				oyp_attribute.value =="0";
			}
			oyp_data = oyp_attribute.key + "=" + encodeURIComponent(oyp_attribute.value);
			oyp_query_params.push(oyp_data);
			
		}
		return oyp_query_params.join("&");
	};

	oyp.collect_clickstream_data = function() {

		var oyp_baseurl = "https://aeopprodvip.acxiom.com/services/clickStream";
		var oyp_query_params = this.getClickStreamData();
		var get_url = oyp_baseurl + "?" + oyp_query_params;

		var x = new XMLHttpRequest();
		x.open("GET", get_url, true);
		x.withCredentials = true;
		x.send();

	};

}

new ClickStreamVars().collect_clickstream_data();


