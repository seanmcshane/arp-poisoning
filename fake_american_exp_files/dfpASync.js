var device_identity_transaction_id = 'HPUSLOGON-57b46b5f-8b0e-4b7e-84bd-14e6c67d827a';
/* sets the device_identity_transaction_id hidden field after the page loaded if it exists.*/
(function() {
	window.addEventListener('load',	
		function() {
			var element = document.getElementById('device_identity_transaction_id');
			if (typeof (element) != 'undefined' && element != null) {
				element.value = device_identity_transaction_id;
			}
		}, false);
})();

if(device_identity_transaction_id && device_identity_transaction_id !='' && device_identity_transaction_id != 'null') {
/* sets transaction_Id */
var _cc = _cc || [];
_cc.push([ 'ci', {'sid' : 'ee490b8fb9a4d570','tid' : 'HPUSLOGON-57b46b5f-8b0e-4b7e-84bd-14e6c67d827a'} ]);
_cc.push(['st', 500]);
_cc.push(['cf', 1514491]);
/* tells the collector to start collecting data */
_cc.push(['mrd', ['https://www.cdn-net.com']]);
_cc.push(['run', 'https://aug.americanexpress.com/collector' ]);

/* appends the InAuth JavaScript about 30k, it is cacheable at client side. */
(function() {
	var c = document.createElement('script');
	        c.setAttribute('src', 'https://aug.americanexpress.com/collector/cc.js?v=4.4.3.1');
	        c.setAttribute('type','text/javascript');
	        c.setAttribute('async','true');
               document.body.appendChild(c);
       })();

}
