

//

'use strict';

var AHP = AHP || {};

AHP.isContentLoaded = false;
AHP.softLaunch = false;
AHP.currentTimeDate = null;
try {
	var timestamp = Date.parse('11/29/2017 23:58:45 GMT');
	if ( isNaN( timestamp ) === true ) {
		timestamp = Date.parse('11/29/2017 08:19:32 MST');
	}
	AHP.currentTimeDate = new Date(timestamp);
} catch (e) {
	AHP.currentTimeDate = new Date();
}

AHP.data = AHP.data || {};
AHP.data.heroTakeover = AHP.data.heroTakeover || {};
AHP.data.hero = AHP.data.hero || {};
try{
	var UN_M = "";
	AHP.data.urgentNotice = {
		m: UN_M
	}
	var HP_UN_M = "Internet Explorer 9<sup>&#174;</sup> and earlier versions are no longer supported by American Express. <a href=\"http://windows.microsoft.com/en-us/internet-explorer/download-ie\" title=\"Upgrade your browser\">Upgrade your browser</a> to avoid issues with viewing and managing your account online.";
} catch(e) {}

try {
	AHP.data.heroDefault = 40126;AHP.data.heroTakeover.id = "40140";AHP.data.heroTakeover.dts = "06/24/2015 00:00:00 MST";AHP.data.heroTakeover.dte = "06/26/2015 23:59:59 MST";AHP.data.heroes = {
	 "list": [
		{
			"id":"40217"
			,"filter":"cm"
			,"dts":"11/26/2017 00:00:00 MST"
			,"dte":"11/30/2017 23:59:00 MST"
		}
		, {
			"id":"40216"
			,"filter":"cm,pr"
			,"dts":"11/26/2017 00:00:00 MST"
			,"dte":"12/22/2017 23:59:00 MST"
		}
		, {
			"id":"40213"
			,"filter":"cm"
			,"dts":"11/26/2017 00:00:00 MST"
			,"dte":"11/31/2017 23:59:00 MST"
		}
		, {
			"id":"40212"
			,"filter":"pr"
			,"dts":"11/29/2017 00:00:00 MST"
			,"dte":"01/16/2018 23:59:00 MST"
		}
	]
};


} catch(e) {}

